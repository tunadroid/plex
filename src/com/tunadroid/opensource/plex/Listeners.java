package com.tunadroid.opensource.plex;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

public class Listeners implements ActionListener{
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand() == "login") {
			String result = Methods.connectURL("http://lf.sdf.org/plex/usrs/lgn.php?eml=" + Core.email.getText() + "&psswd=" + Core.password.getPassword().toString());
			if(result.contains("err")) {
				System.out.println("Failed to login. ");
				
			}
			
			else if(result.contains("false")) {
				System.out.println("Invalid Credentials. ");
				
			}
			
			else {
				Core.UUID = result;
				
			}
		}
		
		if(e.getActionCommand() == "register") {
			try {
				Methods.openWebpage(new URL("http://lf.sdf.org/plex/signup.php"));
				
			} 
			
			catch (MalformedURLException e1) {
				e1.printStackTrace();
				
			}
		}
		
		if(e.getActionCommand() == "forget") {
			try {
				Methods.openWebpage(new URL("http://lf.sdf.org/plex/forget.php"));
				
			} 
			
			catch (MalformedURLException e1) {
				e1.printStackTrace();
				
			}
		}
		
		if(e.getActionCommand() == "quit") {
			Methods.quit();
			
		}
	}
}
