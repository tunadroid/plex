package com.tunadroid.opensource.plex;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Core {
	//The core is what runs on startup
	static JFrame loginScreen = new JFrame("Plex");
	static JFrame loadingWindow = new JFrame("");
	
	static TargetDataLine line; //Initialize line to be used as mic
	
	static AudioFormat audioFormat;
	
	static Color PlexTheme = new Color(0, 255, 33);
	
	static DataLine.Info dataLineInfo;
	
	static byte[] data;
	
	static JTextField email = new JTextField(25);
	
	static JPasswordField password = new JPasswordField(25);
	
	public static String UUID = null;
	
	public static void main(String args[]) {
		System.out.println("Creating windows...");
		//loading screen
		JPanel loadingScreen = new JPanel();
		JLabel loading = new JLabel("Loading...");
		loading.setFont(new Font(loading.getFont().getName(), Font.BOLD, 60));
		loading.setForeground(Color.WHITE);
		loadingScreen.setBackground(PlexTheme);
		loadingScreen.add(loading);
		loadingWindow.add(loadingScreen);
		loadingWindow.setSize(new Dimension (300, 120));
		loadingWindow.setResizable(false);
		loadingWindow.getContentPane().setBackground(PlexTheme);
		loadingWindow.setLocationRelativeTo(null);
		loadingWindow.setAlwaysOnTop(true);
		loadingWindow.setUndecorated(true);
		loadingWindow.setVisible(true);
		//login screen
		JLabel background = new JLabel();
		background.setIcon(new ImageIcon("res//logo.png")); 
		JPanel loginPane = new JPanel();
		JPanel form = new JPanel();
		JPanel actionRow = new JPanel();
		loginPane.setBackground(PlexTheme);
		loginPane.setSize(new Dimension(420, 480));
		loginPane.setLocation(216, 0);
		JLabel label = new JLabel("Login:");	
		label.setFont(new Font(label.getFont().getName(), Font.BOLD, 60));
		label.setForeground(Color.WHITE);
        form.setSize(new Dimension(100, 480));
        form.setLayout(new BoxLayout (form, BoxLayout.Y_AXIS));
        email.setFont(email.getFont().deriveFont(20f));
        password.setFont(password.getFont().deriveFont(20f));
		loginPane.add(label);
		JLabel eml = new JLabel("Email: ");
		eml.setFont(new Font(label.getFont().getName(), Font.BOLD, 20));
		eml.setForeground(Color.WHITE);
		JLabel psswd = new JLabel("Password: ");
		psswd.setFont(new Font(label.getFont().getName(), Font.BOLD, 20));
		psswd.setForeground(Color.WHITE);
		JLabel space = new JLabel("   ");
		JLabel space2 = new JLabel("   ");
		JLabel space3 = new JLabel("   ");
		JButton submit = new JButton("Login");
		submit.addActionListener(new Listeners());
		submit.setActionCommand("login");
		JButton register = new JButton("Sign Up");
		register.addActionListener(new Listeners());
		register.setActionCommand("register");
		JButton forget = new JButton("Forgot Password?");
		forget.addActionListener(new Listeners());
		forget.setActionCommand("forget");
		JButton quit = new JButton("Quit");
		quit.addActionListener(new Listeners());
		quit.setActionCommand("quit");
		actionRow.setLayout(new BoxLayout (actionRow, BoxLayout.X_AXIS));
		actionRow.add(submit);
		actionRow.add(space);
		actionRow.add(forget);
		actionRow.add(space2);
		actionRow.add(register);
		actionRow.add(space3);
		actionRow.add(quit);
		actionRow.setBackground(PlexTheme);
		form.add(eml);
		form.add(email);
		form.add(new JLabel(" "));
		form.add(psswd);
		form.add(password);
		form.add(new JLabel(" "));
		form.add(actionRow);
		form.setBackground(PlexTheme);
		loginPane.add(form);
		loginScreen.add(loginPane);
		loginScreen.add(background);
		loginScreen.setSize(new Dimension (640, 480));
		loginScreen.setResizable(false);
		loginScreen.addWindowListener(new WindowAdapter() {
  			public void windowClosing(WindowEvent e) {
  				System.exit(0);
    		
  			}
  		});		
		
		loginScreen.getContentPane().setBackground(Color.WHITE);
		loginScreen.setLocationRelativeTo(null);
		System.out.println("Window created. ");
		System.out.println("Launching initialization...");
		init();
		loginScreen.setVisible(true);
		loadingWindow.setVisible(false);
		
	}
	
	public static void init() {
		System.out.println("Initialization launched. ");
		System.out.println("Initializing mic... ");
		audioFormat = Methods.getAudioFormat();
		dataLineInfo = new DataLine.Info(TargetDataLine.class, Core.audioFormat);
		try {
			line = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
			
		} 
		
		catch (LineUnavailableException e) {
			e.printStackTrace();
			
		}
		
		System.out.println("Mic initialized. ");
		System.out.println("Initializing mic Buffer... ");
		data = new byte[line.getBufferSize() / 5];
		System.out.println("Mic Buffer initialized. ");
		System.out.println("Initialization complete. ");

		
	}
}
