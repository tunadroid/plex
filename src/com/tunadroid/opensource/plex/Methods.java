package com.tunadroid.opensource.plex;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

public class Methods {
	public static AudioFormat getAudioFormat() {
	    float sampleRate = 8000.0F;
	    //OPTIONS: 8000, 11025, 16000, 22050, 44100
	    int sampleSizeInBits = 16;
	    //OPTIONS: 8, 16
	    int channels = 1;
	    //OPTIONS: 1, 2
	    boolean signed = true;
	    //OPTIONS: true, false
	    boolean bigEndian = false;
	    //OPTIONS: true, false
	    return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
	    
	}
	
	public boolean startMic(AudioFormat format) {
		try {
			Core.line.open(format);
			Core.line.start();
			return true;
			
		}
		
		catch (LineUnavailableException e) {
			e.printStackTrace();
			return false;
			
		}
	}
	
	public void stopMic(AudioFormat format) {
			Core.line.stop();
			
	}
	
	public void call() {
		
	}	
	
	public void setStatus() {
		
	}

	public static void quit() {
		setLoading(true);
		//TODO
		setLoading(false);
		System.exit(0);
		
	}

	public static String connectURL(String URL) {
		setLoading(true);
		URL url;
		try {
			url = new URL(URL);
			InputStream is = (InputStream) url.getContent();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = null;
			StringBuffer sb = new StringBuffer();
			while((line = br.readLine()) != null){
				sb.append(line);
			   
			}
			 
			setLoading(false);
			return sb.toString();
			
		} 
		
		catch (MalformedURLException e) {
			e.printStackTrace();
			setLoading(false);
			
		} 
		
		catch (IOException e) {
			e.printStackTrace();
			setLoading(false);
			
		}
		
		return "err";
		
	}
	
	public static void setLoading(Boolean val) {
		Core.loadingWindow.setVisible(val);
		
	}
	
	public static void openWebpage(URL url) {
		setLoading(true);
	    URI uri;
		try {
			uri = url.toURI();
		    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
		        try {
		            desktop.browse(uri);
		            
		        } 
		        
		        catch (Exception e) {
		            e.printStackTrace();
		            
		        }
		    }
		} 
		
		catch (URISyntaxException e1) {
			e1.printStackTrace();
			
		}
		
		setLoading(false);
		
	}
}
